package com.example.filedemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.filedemo.model.SocieteLiv;

@Repository
public interface SocieteLivRepository extends CrudRepository<SocieteLiv, Long> {

}
