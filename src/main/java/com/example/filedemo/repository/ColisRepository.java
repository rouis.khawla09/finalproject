package com.example.filedemo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.filedemo.model.Colis;
import com.example.filedemo.model.Personnel;

@Repository
public interface ColisRepository extends JpaRepository < Colis , Long >{

	@Query("SELECT s FROM Colis s WHERE s.etat = :etat ")
	  public List<Colis> findColisByEtat(@Param("etat") String etat);
	
	@Query(value="SELECT * FROM Colis s WHERE s.fournisseur_iduser  = :id ", nativeQuery = true)
	  public List<Colis> findByFournisseur_id(@Param("id") Long id);
	
	@Query("SELECT s FROM Colis s WHERE s.etat = :etat AND fournisseur_iduser = :fournisseur_id ")
	  public List<Colis> findByFournisseurAndEtat(@Param("etat") String etat, @Param("fournisseur_id") Long fournisseur_id ); 
	
	
	@Query(value="SELECT c.etat , r.revtstmp FROM revinfo r , Colis_aud c WHERE c.reference = :reference and c.rev = r.rev", nativeQuery = true)
	 public List<HistoStateOnly> getColisAud(Long reference);
	
	 public static interface HistoStateOnly {

	     String getEtat();

	     Long getRevtstmp();

	  }
	 @Query("SELECT count(*) FROM Colis s WHERE s.etat=:etat AND fournisseur_iduser = :fournisseur_id  ")

	 public int  countByEtat( @Param("fournisseur_id") Long fournisseur_id, @Param("etat") String etat);
	 
	 @Query(value = "SELECT * FROM Colis s WHERE s.reference IN :inputList " , 
		     nativeQuery = true)
		List<Colis> findByObjectList( List<Long> inputList);
	 
	 @Query(value = "SELECT * FROM Colis c WHERE c.bar_code  LIKE :bar_code " , nativeQuery = true)
		Colis findColisByBarCode( String bar_code);

	 @Query(value = "SELECT * FROM Colis c WHERE c.runsheet_code_runsheet = :runsheet_code " , nativeQuery = true)
	 List<Colis> findColisByRunsheet_code( Long runsheet_code);

	@Query(value="SELECT SUM(c.cod) FROM colis c WHERE c.runsheet_code_runsheet= :id ", nativeQuery = true)
	public float totalCodPerRunsheet(@Param("id") Long id);

	@Query(value="SELECT * FROM colis c WHERE c.fournisseur_iduser IN (SELECT f.iduser FROM fournisseur f where f.societe_liv_id =:id) ", nativeQuery = true)
	public List<Colis> getColisBySocieteLiv(@Param("id") Long id);

	@Query(value="SELECT count(*) FROM Colis c WHERE c.etat= :etat AND c.fournisseur_iduser IN (SELECT f.iduser FROM fournisseur f WHERE f.societe_liv_id = :id)", nativeQuery = true)
	public int countColisByEtatAndSocieteLiv(@Param("id") Long id, @Param("etat") String etat);

	@Query(value="SELECT * FROM colis c WHERE ( c.fournisseur_iduser IN (SELECT f.iduser FROM fournisseur f where f.societe_liv_id =:id)) and c.etat = :etat ", nativeQuery = true)
	public List<Colis> getColisBySocieteLivAndEtat(@Param("id") Long id, @Param("etat") String etat);
}
