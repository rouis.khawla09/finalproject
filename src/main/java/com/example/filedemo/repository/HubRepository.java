package com.example.filedemo.repository;

import com.example.filedemo.model.Fournisseur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.filedemo.model.Hub;

import java.util.List;


@Repository
public interface HubRepository extends CrudRepository<Hub, Long> {
    @Query(value="SELECT * FROM hub h WHERE h.societe_liv_id=:id ", nativeQuery = true)
    public  List<Hub> getHubBySocieteLiv(@Param("id") Long id);

}
