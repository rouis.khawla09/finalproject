package com.example.filedemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.filedemo.model.Console;

@Repository
public interface ConsoleRepository extends CrudRepository<Console, Long> {

}
