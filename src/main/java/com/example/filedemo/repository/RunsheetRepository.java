package com.example.filedemo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.filedemo.model.Runsheet;
import com.example.filedemo.model.Colis;
@Repository
public interface RunsheetRepository extends CrudRepository<Runsheet, Long>{
	
	@Query(value = "SELECT * FROM Colis s WHERE s.reference IN :inputList " , 
		     nativeQuery = true)
		List<Colis> findByObjectList(/*@Param("inputList")*/ List<Long> inputList);

	 @Query(value = "SELECT * FROM Colis s WHERE s.reference IN :inputList " , 
		     nativeQuery = true)
		List<Colis> donner(/*@Param("inputList")*/ List<Long> inputList);

	@Query(value = "SELECT * FROM runsheet r WHERE r.personnel_iduser IN " +
			"(SELECT p.iduser from personnel p WHERE (p.hub_id_hub IN " +
			"(SELECT h.id_hub from hub h WHERE h.societe_liv_id = :id))) " ,
			nativeQuery = true)

	List<Runsheet> getRunsheetBySocieteLiv(@Param("id") Long id);
}
