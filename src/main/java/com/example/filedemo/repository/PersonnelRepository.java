package com.example.filedemo.repository;

import java.util.List;

import com.example.filedemo.model.Fournisseur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.filedemo.model.Colis;
import com.example.filedemo.model.Personnel;

@Repository
public interface PersonnelRepository extends CrudRepository<Personnel, Long> {

	void save(Colis colis)
	;
	@Query(value="SELECT CONCAT(nom,' ', prenom) AS nomCompletLivreur, iduser FROM personnel WHERE role_personnel = 0", nativeQuery = true)
	 public List<livreurList> getLivreurList();
	
	 public static interface livreurList {

	     String getNomCompletLivreur();

	     Long getId();

	  }

	@Query(value="SELECT * FROM personnel p WHERE (select h.societe_liv_id from hub h WHERE p.hub_id_hub= h.id_hub) =:id ", nativeQuery = true)
	public List<Personnel> getPersonnelBySocieteLiv(@Param("id") Long id);

}