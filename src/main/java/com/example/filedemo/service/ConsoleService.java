package com.example.filedemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.filedemo.model.Console;
import com.example.filedemo.repository.ConsoleRepository;



@Service
public class ConsoleService {

	@Autowired
    ConsoleRepository crepository ;
	
	public List<Console> retrieveAllConsoles() {
        // TODO Auto-generated method stub
        return (List<Console>) crepository.findAll();

    }



    public Console addConsole(Console p) {
        // TODO Auto-generated method stub
        return (crepository.save(p));
    }


    public void deleteConsole(Long id) {
        // TODO Auto-generated method stub
    	crepository.deleteById(id);
    }


    public Console updateConsole(Console p) {
        // TODO Auto-generated method stub
        return (crepository.save(p));
    }


    public Console retrieveConsole(String id) {
        // TODO Auto-generated method stub
        return (crepository.findById(Long.parseLong(id)).orElse(null));
    }
}
