package com.example.filedemo.service;

import java.util.Date;
import java.util.List;

import com.example.filedemo.model.Employe;




public interface IEmployeService {
	
	public int ajouterEmploye(Employe employe);
	public void mettreAjourEmailByEmployeId(String email, int employeId);
	public Employe getEmployePrenomById(int employeId);
	public int getNombreEmployeJPQL();
	public List<String> getAllEmployeNamesJPQL();
	public List<Employe> getAllEmployes();

	
	
	

	
}
