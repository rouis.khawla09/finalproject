package com.example.filedemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.filedemo.model.Fournisseur;
import com.example.filedemo.model.Hub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.filedemo.model.Personnel;
import com.example.filedemo.repository.HubRepository;
import com.example.filedemo.repository.PersonnelRepository;
import com.example.filedemo.repository.PersonnelRepository.livreurList;

@Service
public class PersonnelService {

	@Autowired
    PersonnelRepository prepository ;
	@Autowired
	HubRepository hb;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	EmailSender emailSender;
	
	public List<Personnel> retrieveAllPersonnels() {
        // TODO Auto-generated method stub
        return (List<Personnel>) prepository.findAll();

    }

    public List<Personnel> getPersonnelBySocieteLiv(Long id)
    {
        return prepository.getPersonnelBySocieteLiv(id);
    }
    
    public List<Personnel> getPersonnelBySocieteLiv2(Long id)
    { List<Hub> list=hb.getHubBySocieteLiv(id);
    ArrayList<Personnel> listP = new ArrayList<Personnel>();
    list.forEach(h -> listP.addAll(retrieveAllPersonnels().stream().filter(p -> p.getHub().getId_hub()==h.getId_hub()).collect(Collectors.toList()))
    	
    );
   return listP;
    }
    

    public Personnel addPersonnel(Personnel p) {
        // TODO Auto-generated method stub
        return (prepository.save(p));
    }

    

    public void deletePersonnel(Long id) {
        // TODO Auto-generated method stub
        prepository.deleteById(id);
    }


    public Personnel updatePersonnel(Personnel p) {
        // TODO Auto-generated method stub
        return (prepository.save(p));
    }


    public Personnel retrievePersonnel(String id) {
        // TODO Auto-generated method stub
        return (prepository.findById(Long.parseLong(id)).orElse(null));
    }
    
    public List<livreurList> getLivreurList()
    {
    	return (prepository.getLivreurList());
    }
}
