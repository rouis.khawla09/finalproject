package com.example.filedemo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.filedemo.model.User;
import com.example.filedemo.repository.UserRepository;

@Service
public class UserService implements Iuserservice{

	@Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private UserRepository userrep ;

	@Override
	public User loadUserByUsername(String username) {
		// TODO Auto-generated method stub
		 return userrep.findByUsername(username);
	}
	
	
	public void saveUser(User user) {
		
		
     String hashPW =bCryptPasswordEncoder.encode(user.getPassword());
     user.setPassword(hashPW);
     userrep.save(user);

          return ;

	}


	@Override
	public ArrayList<GrantedAuthority> loadRoleByUsername(String username) {
		User user = loadUserByUsername(username);
		 ArrayList<GrantedAuthority> authorities = new ArrayList<>();
	     authorities.add(new SimpleGrantedAuthority(user.getRoleUser().toString()));
		 
//		 user.getRole().forEach(r -> {
//	        authorities.add(new SimpleGrantedAuthority(r.getRole()));
	       // });
		return authorities;
	}

}
