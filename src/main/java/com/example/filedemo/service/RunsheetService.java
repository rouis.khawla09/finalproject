package com.example.filedemo.service;

import java.util.List;
import java.util.Optional;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.filedemo.controller.ColisController;
import com.example.filedemo.model.Colis;
import com.example.filedemo.model.Runsheet;
import com.example.filedemo.repository.RunsheetRepository;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;


@Service
public class RunsheetService {

	@Autowired
    RunsheetRepository prepository ;
	
	@Autowired
	private ColisController coliscontroller ;

	@Autowired
	private ColisService colisService ;
	
	public List<Runsheet> retrieveAllRunsheets() {
        // TODO Auto-generated method stub
        return (List<Runsheet>) prepository.findAll();

    }



    public Runsheet addRunsheet(Runsheet runsheet) {
		runsheet = prepository.save(runsheet);
		runsheet.setBar_code(generateRunsheetBarCode(runsheet.getCode_runsheet()));
		return (prepository.save(runsheet));
    }



    public void deleteRunsheet(Long id) {
    	List<Colis> listColis = coliscontroller.findColisByRunsheet_code(id);
    	for(Colis colis : listColis)
    	{
    		colis.setRunsheet(null);
    		coliscontroller.updateColis(colis);
    	}
        prepository.deleteById(id);
    }


    public Runsheet updateRunsheet(Runsheet runsheet) {


		float total = colisService.totalCodPerRunsheet(runsheet.getCode_runsheet());
		if (total >0)
		{runsheet.setTotal_prix(total);}
		else
		{runsheet.setTotal_prix(0);}
		runsheet.setBar_code(generateRunsheetBarCode(runsheet.getCode_runsheet()));
		return (prepository.save(runsheet));
    }


    public Runsheet retrieveRunsheet(String id) {
        // TODO Auto-generated method stub
        return (prepository.findById(Long.parseLong(id)).orElse(null));
    }
	
    public Optional<Runsheet> findById(Long code_runsheet) {
		return prepository.findById(code_runsheet);
	}	
	
	public List<Colis> findByObjectList(List<Long> inputList)
	{
	List<Colis> List1 = prepository.findByObjectList(inputList);
	return List1 ;
	}
	
	public String addColisToRunsheet(List<String> bar_codeList ,  Long code_runsheet )
	{
		Runsheet testRunsheet = findById(code_runsheet).orElse(new Runsheet()) ;
		float totalRunsheet=0;
		for (String bar_code : bar_codeList) {
			

			Colis testColis = coliscontroller.findColisByBarCode(bar_code) ; 
			totalRunsheet += testColis.getCod();
			testColis.setRunsheet(testRunsheet);
			
			coliscontroller.updateColis(testColis);
		    
		}

		testRunsheet.setTotal_prix(totalRunsheet);
		updateRunsheet(testRunsheet);
		return "OK!" ; 
		
	}
	
	
    
	public String generateRunsheetBarCode (Long code_runsheet)
	{
		Runsheet RunBarCode = findById(code_runsheet).orElse(new Runsheet());
		String text = RunBarCode.toRunsheetBarCode(); 
		String path = "C:\\Users\\Imen\\Desktop\\my stuff\\Cours\\2020-2021\\Stage Ah-Co\\Backend-spring\\src\\main\\java\\imageBarCodeRunsheet\\"
				+text+".jpg";
		try {
				Code128Writer writer = new Code128Writer(); 
				BitMatrix matrix = writer.encode(text, BarcodeFormat.CODE_128, 400, 90);
				MatrixToImageWriter.writeToPath(matrix, "jpg", Paths.get(path));
				System.out.println ("Barcode created");
		} catch (Exception e ) 
				{
					System.out.println ("Error while creating barcode");
				}
				
		return text;
	}
	public List<Runsheet> getRunsheetBySocieteLiv(Long id) {

		return (prepository.getRunsheetBySocieteLiv(id));
	}
    
}
