package com.example.filedemo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.filedemo.model.Employe;
import com.example.filedemo.repository.EmployeRepository;



@Service
public class EmployeServiceImpl implements IEmployeService {

	private static final Logger l = LogManager.getLogger(EmployeServiceImpl.class);
	
	@Autowired
	EmployeRepository employeRepository;
	
	@Override
	public int ajouterEmploye(Employe employe) {
		try {
			l.info("in ajouter Employe");
			l.debug("Je vais commencer l'ajout");
			employeRepository.save(employe);
			l.info("out ajouter Employe");
			return employe.getId();
		}
		catch (Exception e) { l.error("Erreur dans ajouterEmploye() : " , e); }
		
		return employe.getId();	
	}
	@Override
	public void mettreAjourEmailByEmployeId(String email, int employeId) {
		
		try {
			l.info("in  mettreAjourEmailByEmployeId");
			l.debug("Je vais commencer la mise à jour");
			Optional<Employe> optional = employeRepository.findById(employeId);
			if (optional.isPresent()) {
				Employe value = optional.get();
				value.setEmail(email);
				employeRepository.save(value);
			}
			l.info("out Mise à jour ");
			}
		catch (Exception e) { l.error("Erreur dans mettreAjourEmailByEmployeId() : " , e); }
	}
	
	@Override
	public Employe getEmployePrenomById(int employeId) {

		Optional<Employe> optional = employeRepository.findById(employeId);
		
 		try {
 			l.info("in  getEmployePrenomById");
 			l.debug("je vais récuperer l'emp");
 			if (optional.isPresent()) {
 				Employe value = optional.get();
 				 value.getPrenom();
 			}
 			l.info("out de getEmployePrenomById ");
 			}
 			catch (Exception e) { l.error("Erreur dans getEmployePrenomById() : " , e); }
		
 		return null;
	}


	
	@Override
	public int getNombreEmployeJPQL() {
		
		try {
			l.info("in  getNombreEmployeJPQL");
			l.debug("je vais recuperer le nombre d'emp");
			l.info("out de getNombreEmployeJPQL ");
			return employeRepository.countemp();
			}
			catch (Exception e) { l.error("Erreur dans getNombreEmployeJPQL() : " , e); }
		return employeRepository.countemp();
	}
	@Override
	public List<String> getAllEmployeNamesJPQL() {
		
		
		try {
			l.info("in  getAllEmployeNamesJPQL");
			l.debug("je vais recuperer all employes names with jpql");
			l.info("out de getAllEmployeNamesJPQL ");
			return employeRepository.employeNames();
			}
			catch (Exception e) { l.error("Erreur dans getAllEmployeNamesJPQL() : " , e); }
		return employeRepository.employeNames();
	}

	
	@Override
	public List<Employe> getAllEmployes() {

		try {
			l.info("in  getAllEmployes");
			l.debug("je vais recuperer la liste des emps");
			l.info("out de getAllEmployes ");
			return (List<Employe>) employeRepository.findAll();
			}
			catch (Exception e) { l.error("Erreur dans getAllEmployes() : " , e); }
		return (List<Employe>) employeRepository.findAll();
	}

	
}
