package com.example.filedemo.service;

import java.util.List;

import com.example.filedemo.model.Fournisseur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.filedemo.controller.ColisController;
import com.example.filedemo.model.Colis;
import com.example.filedemo.model.Hub;
import com.example.filedemo.repository.HubRepository;



@Service
public class HubService {

	@Autowired
    private HubRepository hubrepository ;
	
	@Autowired
	private ColisService colisService ;
	
	public List<Hub> retrieveAllHubs() {
        // TODO Auto-generated method stub
        return (List<Hub>) hubrepository.findAll();

    }



    public Hub addHub(Hub p) {
        // TODO Auto-generated method stub
        return (hubrepository.save(p));
    }


    public void deleteHub(Long id) {
        // TODO Auto-generated method stub
    	hubrepository.deleteById(id);
    }


    public Hub updateHub(Hub p) {
        // TODO Auto-generated method stub
        return (hubrepository.save(p));
    }


    public Hub retrieveHub(String id) {
        // TODO Auto-generated method stub
        return (hubrepository.findById(Long.parseLong(id)).orElse(null));
    }
    
    public String affectationColisToHub(Long reference )
	{   
		
		Colis colis = colisService.findById(reference).orElse(new Colis()) ;
		
		List<Hub> inputList = retrieveAllHubs() ;

		for (Hub hub : inputList) {
			
			for (String gou : hub.getGouvernorat_lie()) {
				if ( gou.equals(colis.getGouvernorat() )) {
					colis.setHub(hub);
					
				 } 
				}
			}
		
		colisService.updateColis(colis) ; 
		return "OK!" ; 	
	}

	public List<Hub> getHubBySocieteLiv(Long id)
	{
		return hubrepository.getHubBySocieteLiv(id);
	}
	
}
