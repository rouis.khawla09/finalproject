package com.example.filedemo.service;





import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.filedemo.controller.ColisController;
import com.example.filedemo.model.Colis;
import com.example.filedemo.model.Fournisseur;
import com.example.filedemo.model.User;
import com.example.filedemo.repository.ColisRepository;
import com.example.filedemo.repository.FournisseurRepository;

@Service
public class FournisseurService {

	

	@Autowired
	private final FournisseurRepository fournisseurRepository ;
	
	@Autowired
	private ColisService colisservice;
	
	@Autowired
	private ColisRepository colisRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	EmailSender emailSender;
	
	
	@Autowired  
	public FournisseurService (FournisseurRepository fournisseurRepository)
	{ this.fournisseurRepository = fournisseurRepository ; }
	
	

	//Get Fournisseur By Id
	public Optional<Fournisseur> findById(Long id) {
		return fournisseurRepository.findById(id);
	}	
	

	
	//save
	public Fournisseur saveFournisseur(Fournisseur fournisseur )
	{
		return fournisseurRepository.save(fournisseur);
    }
	
	
	
	//update
	public Fournisseur updateFournisseur(Fournisseur fournisseur)
	{
	  return fournisseurRepository.save(fournisseur) ;
	}
	
	
	
	//lister les F 
	public List<Fournisseur> getFournisseurBySocieteLiv(Long id)
	{
		return fournisseurRepository.getFournisseurBySocieteLiv(id);	
	}
	//lister les F delited with societeLiv = 1
		public List<Fournisseur> getFournisseurIsDeleted()
		{
//			 List<Fournisseur> l= getAllFournisseur();
//			 return l.stream().filter(f -> f.getSocieteLiv().getId()== 1 )
	return (List<Fournisseur>) fournisseurRepository.findByIsDeletedFalse().stream().filter(f -> f.getSocieteLiv().getId()== 2).collect(Collectors.toList());
			}
		
		//lister les F delited with societeLiv = id 
				public List<Fournisseur> getFournisseurIsDeletedFalseByIdSL(long id)
				{
//					 List<Fournisseur> l= getAllFournisseur();
//					 return l.stream().filter(f -> f.getSocieteLiv().getId()== 1 )
			return (List<Fournisseur>) fournisseurRepository.findByIsDeletedFalse().stream().filter(f -> f.getSocieteLiv().getId()== id).collect(Collectors.toList());
					}
	
	
	//get all F
	public List<Fournisseur> getAllFournisseur()
	{
		return fournisseurRepository.findAll();	
	}
	
	
	
	//supprimer un F
	public void deleteLogiqueFournisseur(Long id)
	{
		Fournisseur deletedFournisseur = findById(id).get();
		
		deletedFournisseur.isDeleted=true;
		List<Colis> listColis = colisservice.findAllColisByFournisseur(id);
		
		for (Colis colis : listColis)
		{
			colis.setFournisseur(null);
			colisRepository.save(colis);
		}
		updateFournisseur(deletedFournisseur) ;
	}

	public Object updateFournisseur(Fournisseur e, Long id) {


		return  fournisseurRepository.updateFournisseur(e.getNom_societe(), e.getNom_f(), e.prenom_f, e.getTel_f(), e.getCin(), e.getEmail_f(), e.getDate_fin_contrat(), e.getAdresse_societe(), e.getGouvernorat_societe(), e.getLocalite_societe(), e.getDelegation_societe(), e.getCode_postal_societe(), e.getAdresse_livraison(), e.getGouvernorat_livraison(), e.getLocalite_livraison(), e.getDelegation_livraison(), e.getCode_postal_livraison(), e.getPrix_livraison(), e.getPrix_retour(), e.getLogo(), e.getPassword(), e.getIduser());
	}


	public Fournisseur saveFour(Fournisseur four ) {


		String hashPW =bCryptPasswordEncoder.encode(four.getCin());
		four.setPassword(hashPW);
		Fournisseur savedFournisseur = fournisseurRepository.save(four);

		emailSender.send(
				four.getEmail_f(),
				buildEmail(four.getNom_f(), four.getCin()));
		return savedFournisseur;

	}

	public void update(Fournisseur u) {


		//	Fournisseur fourrr = fou.findById(u.getId()).get();

		String pas = new BCryptPasswordEncoder().encode(u.getPassword());

		u.setPassword(pas);

		fournisseurRepository.save(u);

	}

	public void changePassWord(Long id, String pas) {

		Fournisseur	 u = fournisseurRepository.findById(id).get();

		if (u != null) {

			u.setPassword(pas);

			this.update(u);
		}}


	private String buildEmail(String name, String pasword) {
		return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
				"\n" +
				"<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
				"\n" +
				"  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
				"    <tbody><tr>\n" +
				"      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
				"        \n" +
				"        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
				"          <tbody><tr>\n" +
				"            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
				"                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
				"                  <tbody><tr>\n" +
				"                    <td style=\"padding-left:10px\">\n" +
				"                  \n" +
				"                    </td>\n" +
				"                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
				"                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
				"                    </td>\n" +
				"                  </tr>\n" +
				"                </tbody></table>\n" +
				"              </a>\n" +
				"            </td>\n" +
				"          </tr>\n" +
				"        </tbody></table>\n" +
				"        \n" +
				"      </td>\n" +
				"    </tr>\n" +
				"  </tbody></table>\n" +
				"  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
				"    <tbody><tr>\n" +
				"      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
				"      <td>\n" +
				"        \n" +
				"                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
				"                  <tbody><tr>\n" +
				"                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
				"                  </tr>\n" +
				"                </tbody></table>\n" +
				"        \n" +
				"      </td>\n" +
				"      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
				"    </tr>\n" +
				"  </tbody></table>\n" +
				"\n" +
				"\n" +
				"\n" +
				"  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
				"    <tbody><tr>\n" +
				"      <td height=\"30\"><br></td>\n" +
				"    </tr>\n" +
				"    <tr>\n" +
				"      <td width=\"10\" valign=\"middle\"><br></td>\n" +
				"      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
				"        \n" +
				"            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",<p>username:" + name + "<p/></p>password: "+ pasword +"<p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">  </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> </a> </p></blockquote>\n  <p>See you soon</p>" +
				"        \n" +
				"      </td>\n" +
				"      <td width=\"10\" valign=\"middle\"><br></td>\n" +
				"    </tr>\n" +
				"    <tr>\n" +
				"      <td height=\"30\"><br></td>\n" +
				"    </tr>\n" +
				"  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
				"\n" +
				"</div></div>";
	}

}
