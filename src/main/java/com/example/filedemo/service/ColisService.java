package com.example.filedemo.service;

import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.filedemo.controller.FournisseurController;
import com.example.filedemo.controller.HubController;
import com.example.filedemo.model.Colis;
import com.example.filedemo.model.Fournisseur;
import com.example.filedemo.model.Personnel;
import com.example.filedemo.repository.ColisRepository;
import com.example.filedemo.repository.ColisRepository.HistoStateOnly;
import com.example.filedemo.repository.PersonnelRepository;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

@Service
public class ColisService {

	@Autowired
	private FournisseurController control ; 
	
	@Autowired
	private HubService hubService ; 
	
	@Autowired
	private final ColisRepository colisRepository ;
	
	@Autowired  
	public ColisService (ColisRepository colisRepository)
	{ this.colisRepository = colisRepository ; }
	

	//save Colis
	public Colis saveColis(Colis colis )
	{
		Colis colisTest = colisRepository.save(colis);
		colisTest.setBar_code(colisTest.toColisBarCode());
		generateColisBarCode(colis.getReference());
		hubService.affectationColisToHub(colisTest.getReference());
		return colisTest;
    }
	
	
	//update Colis
	public Colis updateColis(Colis colis )
	{
		colis.setFournisseur(control.findById(colis.fournisseur.getIduser()).orElse(new Fournisseur()));
		generateColisBarCode(colis.getReference());
		return colisRepository.save(colis) ;
	}
		
		
		
	//lister les colis 
	public List<Colis> findAllColisByFournisseur(Long id)
	{
		return colisRepository.findByFournisseur_id(id);	
	}
		
	// liste des audits 
		
	public List<HistoStateOnly> getColisAud(Long reference)
	{
		return colisRepository.getColisAud(reference);
		
	}
		
	//supprimer un colis 
	public void deleteColis(Long reference)
	{
		colisRepository.deleteById(reference) ;
	}
		
	//findColisCreé
	public List<Colis> findColisByEtat(String etat)
	{
		List<Colis> list = colisRepository.findColisByEtat(etat);
		return list;
	}
	
	public List<Colis> findByObjectList(List<Long> inputList)
	{
		List<Colis> List1 =  colisRepository.findByObjectList(inputList);
		return List1 ;
	}
	
	
	public Colis findColisByBarCode(String bar_code)
	{
		Colis List1 =  colisRepository.findColisByBarCode(bar_code);
		return List1 ;
	}
	
	//findColis by id 
	public Optional<Colis> findById(Long referene)
		
	{
		return colisRepository.findById(referene);
	}
	
	public List<Colis> findColisByRunsheet_code(Long runsheet_code)
	
	{
		return colisRepository.findColisByRunsheet_code(runsheet_code);
	}
	//findColis by id and by etat 
	public List<Colis> findByFournisseurAndEtat( String etat, Long referene)
				
	{
		return colisRepository.findByFournisseurAndEtat(etat, referene);
	}	
	
	public String generateColisBarCode (Long reference)
	{
		Colis colisBarCode = findById(reference).orElse(new Colis());
		String text = colisBarCode.toColisBarCode(); 
		String path = "C:\\Users\\NOUR\\Documents\\GitHub\\Stagerepo\\src\\main\\java\\imageBarCodeColis\\"
				+ text+".jpg";
		try {
				Code128Writer writer = new Code128Writer(); 
				BitMatrix matrix = writer.encode(text, BarcodeFormat.CODE_128, 400, 90);
				MatrixToImageWriter.writeToPath(matrix, "jpg", Paths.get(path));
				System.out.println ("Barcode created");
		} catch (Exception e ) 
				{
					System.out.println ("Error while creating barcode");
				}
				
		return text;
	}
	
	public int countByEtat(Long id , String etat)
	{
	
		return colisRepository.countByEtat(id , etat);
	
	}
	
	public void RemoveColisFromRunsheet(Long reference) {
		
		Colis colis = findById(reference).get();
		colis.setRunsheet(null);
		updateColis(colis);	
	}

	public float totalCodPerRunsheet(Long id) {

		return colisRepository.totalCodPerRunsheet(id);
	}

	public List<Colis> getColisBySocieteLiv(Long id) {

		return colisRepository.getColisBySocieteLiv(id);
	}

	public int countColisByEtatAndSocieteLiv(Long id, String etat) {

		return colisRepository.countColisByEtatAndSocieteLiv(id, etat);
	}

	public List<Colis> getColisBySocieteLivAndEtat(Long id, String etat) {

		return colisRepository.getColisBySocieteLivAndEtat(id, etat);
	}
}
