package com.example.filedemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.filedemo.model.SocieteLiv;
import com.example.filedemo.repository.SocieteLivRepository;


@Service
public class SocieteLivService {

	@Autowired
	SocieteLivRepository slrrepository ;
	
	public List<SocieteLiv> retrieveAllSocieteLivs() {
        // TODO Auto-generated method stub
        return (List<SocieteLiv>) slrrepository.findAll();

    }



    public SocieteLiv addSocieteLiv(SocieteLiv p) {
        // TODO Auto-generated method stub
        return (slrrepository.save(p));
    }


    public void deleteSocieteLiv(Long id) {
        // TODO Auto-generated method stub
    	slrrepository.deleteById(id);
    }


    public SocieteLiv updateSocieteLiv(SocieteLiv p) {
        // TODO Auto-generated method stub
        return (slrrepository.save(p));
    }


    public SocieteLiv retrieveSocieteLiv(String id) {
        // TODO Auto-generated method stub
        return (slrrepository.findById(Long.parseLong(id)).orElse(null));
    }
}
