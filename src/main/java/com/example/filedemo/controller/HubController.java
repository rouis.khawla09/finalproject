package com.example.filedemo.controller;

import java.util.List;

import com.example.filedemo.model.Fournisseur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.filedemo.model.Hub;
import com.example.filedemo.model.Personnel;
import com.example.filedemo.service.HubService;
import com.example.filedemo.service.PersonnelService;

@RestController
public class HubController {

	@Autowired
    HubService hubservice ;
	
	//http://localhost:8080/retrieve-all-Hubs
    @GetMapping("/retrieve-all-Hubs")
    @ResponseBody
    public List<Hub> getHub() {
        List<Hub> list = hubservice.retrieveAllHubs();
        return list;
    }

    // http://localhost:8080/retrieve-Hub/{Hub-id}
    @GetMapping("/retrieve-Hub/{Hub-id}")
    @ResponseBody
    public Hub retrieveHub(@PathVariable("Hub-id") String id) {
        return hubservice.retrieveHub(id);
    }

    // Ajouter  : http://localhost:8080/add-Hub
    @PostMapping("/add-Hub")
    @ResponseBody
    public Hub addHub(@RequestBody Hub p) {
    	Hub p1 = hubservice.addHub(p);
        return p1;
    }



    // http://localhost:8080/remove-Hub/{id}
    @DeleteMapping("/remove-Hub/{id}")
    @ResponseBody
    public void removeHub (@PathVariable("id") Long id) {
    	hubservice.deleteHub(id);
    }

    // http://localhost:8080/modify-Hub
    @PutMapping("/modify-Hub")
    @ResponseBody
    public Hub modifyPersonnel(@RequestBody Hub p) {
        return hubservice.updateHub(p);
    }

    @GetMapping(value="getHubBySocieteLiv/{id}")
    public List<Hub> getHubBySocieteLiv(@PathVariable(value = "id") Long id)
    {
        return hubservice.getHubBySocieteLiv(id) ;
    }
}
