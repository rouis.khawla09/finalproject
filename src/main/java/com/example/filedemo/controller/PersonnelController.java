package com.example.filedemo.controller;

import java.io.File;
import java.util.List;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.filedemo.model.Fournisseur;
import com.example.filedemo.model.Personnel;
import com.example.filedemo.model.Response;
import com.example.filedemo.repository.FournisseurRepository;
import com.example.filedemo.repository.PersonnelRepository;
import com.example.filedemo.repository.PersonnelRepository.livreurList;
import com.example.filedemo.service.PersonnelService;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class PersonnelController {

	@Autowired
    PersonnelService pservice ;
	
	@Autowired
	ServletContext context;
	
	@Autowired 	
	PersonnelRepository  repository;
	
	//http://localhost:8080/retrieve-all-Personnels
    @GetMapping("/retrieve-all-Personnels")
    @ResponseBody
    public List<Personnel> getPersonnel() {
        List<Personnel> list = pservice.retrieveAllPersonnels();
        return list;
    }

    // http://localhost:8080/retrieve-Personnel/{Personnel-id}
    @GetMapping("/retrieve-Personnel/{Personnel-id}")
    @ResponseBody
    public Personnel retrievePersonnel(@PathVariable("Personnel-id") String id) {
        return pservice.retrievePersonnel(id);
    }

    // Ajouter  : http://localhost:8080/add-Personnel
    @PostMapping("/add-Personnel")
    @ResponseBody
    public Personnel addPersonnel(@RequestBody Personnel p) {
    	Personnel p1 = pservice.addPersonnel(p);
        return p1;
    }
    @GetMapping(value="getPersonnelBySocieteLiv/{id}")
    public List<Personnel> getPersonnelBySocieteLiv(@PathVariable(value = "id") Long id)
    {
        return pservice.getPersonnelBySocieteLiv2(id) ;
    }


    // http://localhost:8080/remove-Personnel/{id}
    @DeleteMapping("/remove-Personnel/{id}")
    @ResponseBody
    public void removePersonnel (@PathVariable("id") Long id) {
        pservice.deletePersonnel(id);
    }

    // http://localhost:8080/modify-Personnel
    @PutMapping("/modify-Personnel")
    @ResponseBody
    public Personnel modifyPersonnel(@RequestBody Personnel p) {
        return pservice.updatePersonnel(p);
    }
    
    
    @GetMapping("/getLivreurList")
    public List<livreurList> getLivreurList()
    {
    	return pservice.getLivreurList();
    }

@PostMapping("/addPersonnelWithImage")
public ResponseEntity<Response> createArticle (@RequestParam("file") MultipartFile file,
		 @RequestParam("personnel") String personnel) throws JsonParseException , JsonMappingException , Exception
{
	 System.out.println("Ok .............");
   Personnel testPersonnel = new ObjectMapper().readValue(personnel, Personnel.class);
   boolean isExit = new File(context.getRealPath("/Images/")).exists();
   if (!isExit)
   {
   	new File (context.getRealPath("/Images/")).mkdir();
   	System.out.println("mk dir.............");
   }
   String filename = file.getOriginalFilename();
   String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
   File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
   try
   {
   	System.out.println("Image");
   	 FileUtils.writeByteArrayToFile(serverFile,file.getBytes());
   	 
   }catch(Exception e) {
   	e.printStackTrace();
   }

  
   testPersonnel.setPhoto(newFileName);
   Personnel  savedPersonnel = repository.save(testPersonnel);
   if (savedPersonnel != null)
   {
   	return new ResponseEntity<Response>(new Response (""),HttpStatus.OK);
   }
   else
   {
   	return new ResponseEntity<Response>(new Response ("Personnel not saved"),HttpStatus.BAD_REQUEST);	
   }
}

@GetMapping(path="/ImagePersonnel/{id}")
public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
	Personnel personnel   = repository.findById(id).get();
	System.out.println("get image done");
	 return Files.readAllBytes(Paths.get(context.getRealPath("/Images/")+personnel.getPhoto()));
     	}

    @PutMapping("/updatePersonnelPhoto")
    public ResponseEntity<Response> updatePersonnelPhoto (@RequestParam("file") MultipartFile file,
                                                            @RequestParam("personnel") String personnel) throws JsonParseException , JsonMappingException , Exception
    {

        Personnel testpersonnel = new ObjectMapper().readValue(personnel, Personnel.class);
        boolean isExit = new File(context.getRealPath("/Images/")).exists();
        if (!isExit)
        {
            new File (context.getRealPath("/Images/")).mkdir();
        }
        String filename = file.getOriginalFilename();
        String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
        File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
        try
        {
            System.out.println("Image");
            FileUtils.writeByteArrayToFile(serverFile,file.getBytes());

        }catch(Exception e) {			e.printStackTrace();
        }


        testpersonnel.setPhoto(newFileName);
        Personnel savedPersonnel = modifyPersonnel(testpersonnel);
        if (savedPersonnel != null)
        {
            return new ResponseEntity<Response>(new Response (""),HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<Response>(new Response ("Fournisseur not saved"),HttpStatus.BAD_REQUEST);
        }
    }


}
