package com.example.filedemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.filedemo.model.Console;
import com.example.filedemo.model.SocieteLiv;
import com.example.filedemo.service.ConsoleService;
import com.example.filedemo.service.SocieteLivService;

@RestController
public class SocieteLivController {

	@Autowired
	SocieteLivService pservice ;
	
	//http://localhost:8080/retrieve-all-SocieteLivs
    @GetMapping("/retrieve-all-SocieteLivs")
    @ResponseBody
    public List<SocieteLiv> getSocieteLiv() {
        List<SocieteLiv> list = pservice.retrieveAllSocieteLivs();
        return list;
    }

    // http://localhost:8080/retrieve-SocieteLiv/{SocieteLiv-id}
    @GetMapping("/retrieve-SocieteLiv/{SocieteLiv-id}")
    @ResponseBody
    public SocieteLiv retrieveSocieteLiv(@PathVariable("SocieteLiv-id") String id) {
        return pservice.retrieveSocieteLiv(id);
    }

    // Ajouter  : http://localhost:8080/add-SocieteLiv
    @PostMapping("/add-SocieteLiv")
    @ResponseBody
    public SocieteLiv addSocieteLiv(@RequestBody SocieteLiv p) {
    	SocieteLiv p1 = pservice.addSocieteLiv(p);
        return p1;
    }



    // http://localhost:8080/remove-SocieteLiv/{id}
    @DeleteMapping("/remove-SocieteLiv/{id}")
    @ResponseBody
    public void removeSocieteLiv(@PathVariable("id") Long id) {
        pservice.deleteSocieteLiv(id);
    }

    // http://localhost:8080/modify-SocieteLiv
    @PutMapping("/modify-SocieteLiv")
    @ResponseBody
    public SocieteLiv modifySocieteLiv(@RequestBody SocieteLiv p) {
        return pservice.updateSocieteLiv(p);
    }
    
    
}
