package com.example.filedemo.controller;


import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.io.ByteArrayInputStream;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.filedemo.model.Colis;
import com.example.filedemo.model.PDFGenerator;
import com.example.filedemo.model.PDFGenerator2;
import com.example.filedemo.repository.ColisRepository.HistoStateOnly;
import com.example.filedemo.service.ColisService;

@Controller
@RestController
@CrossOrigin("*")
@RequestMapping

public class ColisController {

	@Autowired 
	private final ColisService colisService ;
	
   @Autowired  
	public ColisController (ColisService colisService)
	{ this.colisService = colisService ;  }

	
//save colis
   @PostMapping(value="saveColis")	
   public Colis saveColis (@RequestBody Colis colis  )
   {
	 System.out.println(" Colis   save works properly ! ") ; 
	 colisService.saveColis(colis);
	 return colis ;
   }
   
   
 //find all Colis by fournisseur
   @GetMapping(value="/getAllColis/{id}")
   public List<Colis> findAllColisByFournisseur(@PathVariable(value ="id") Long id)
   {
	  return colisService.findAllColisByFournisseur(id) ;
   }
  
   
 //find all by fournisseur and state 
   @GetMapping(value="/getColis/{id}/{etat}")
   public List<Colis> findByFournisseurAndEtat(@PathVariable(value = "id") Long id, @PathVariable(value = "etat") String etat)
   {
	  return colisService.findByFournisseurAndEtat(etat , id) ;
   }
   
   
    @PutMapping("updateColis")
    public Colis updateColis( @RequestBody  Colis colis )
   {  
	   return colisService.updateColis(colis);
   }
     
    

    @DeleteMapping("/deleteColis/{reference}")
    public String deleteColis(@PathVariable(value = "reference") Long reference)
    {
   	colisService.deleteColis(reference);
   	return "Colis Deletetd !" ;
    }
    
    @RequestMapping("/createBarCode/{reference}")
    public String generateColisBarCode(@PathVariable(value = "reference") Long reference)
     {
    	String text = colisService.generateColisBarCode(reference);
    	return text ;
     }
   

    @GetMapping("/findColisByEtat/{etat}")
	  
	  @ResponseBody 
	  public  List<Colis> findColisByEtat(@PathVariable(value = "etat") String etat) {
		  List<Colis> list =  colisService.findColisByEtat(etat);
		  return list; 
	 }
  
    @GetMapping("/getColisAudit/{reference}")
	  
	  @ResponseBody 
	  public  List<HistoStateOnly> getColisAud(@PathVariable(value = "reference") Long reference) {
		  return  colisService.getColisAud(reference); 
		   
	 }
  
    @GetMapping("/object")
    // @ResponseBody
     public List<Colis> findByObjectList(@RequestBody  List<Long> inputList  )
     {
     	//List<Long> inputList ; 
     	List<Colis> List1 = colisService.findByObjectList(inputList);
     	return List1 ;
     }
    @GetMapping("/totalCodPerRunsheet/{id}")
    // @ResponseBody
    public float totalCodPerRunsheet(@PathVariable(value = "id") Long id  )
    {

        return colisService.totalCodPerRunsheet(id);

    }
    
    @GetMapping("/findColisByBarCode/{bar_code}")
    @ResponseBody
     public Colis findColisByBarCode(@PathVariable(value = "bar_code") String bar_code )
     {
     	Colis List1 = colisService.findColisByBarCode(bar_code);
     	return List1 ;
     }
  //count by etat 
    @GetMapping("count/{id}/{etat}")
    @ResponseBody
    public int countByEtat(@PathVariable(value = "id") Long id,@PathVariable(value = "etat") String etat )
      {
             return colisService.countByEtat(id, etat) ;
      }
       
       
    @RequestMapping("/findColisById/{reference}")   
	@ResponseBody
	public Optional<Colis> findById(@PathVariable(value = "reference") Long reference)
	{
		return colisService.findById(reference);
		
	}
    
    @RequestMapping("/findColisByRunsheet_code/{runsheet_code}")   
	@ResponseBody
	public List<Colis> findColisByRunsheet_code(@PathVariable(value = "runsheet_code") Long runsheet_code)
	{
		return colisService.findColisByRunsheet_code(runsheet_code);
		
	}
    
    
    @PutMapping(value = "/pdfDecharge",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity colisDechargeReport(@RequestBody  List<Long> inputList ) throws IOException {
        List <Colis> coliss =  colisService.findByObjectList(inputList );

        ByteArrayInputStream bis = PDFGenerator2.colisDechargeReport(coliss);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=customers.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
    
  //pdf1
            @PutMapping(value = "/pdfFactureDordereau",
                    produces = MediaType.APPLICATION_PDF_VALUE)
            public ResponseEntity colissReport(@RequestBody  List<Long> inputList1 ) throws IOException {
            List <Colis> coliss =  colisService.findByObjectList(inputList1 );

            ByteArrayInputStream bis = PDFGenerator.colisPDFReport(coliss);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "inline; filename=customers.pdf");



           return ResponseEntity
                   .ok()
                   .headers(headers)
                   .contentType(MediaType.APPLICATION_PDF)
                   .body(new InputStreamResource(bis));
}




    @RequestMapping("/RemoveColisFromRunsheet/{reference}")   
   	@ResponseBody
    public void RemoveColisFromRunsheet(@PathVariable(value = "reference") Long reference)
    {
    	colisService.RemoveColisFromRunsheet(reference);
    }

    @GetMapping("/getColisBySocieteLiv/{id}")
    public List<Colis> getColisBySocieteLiv(@PathVariable(value = "id") Long id  )
    {
        return colisService.getColisBySocieteLiv(id);
    }

    @GetMapping("/countColisByEtatAndSocieteLiv/{id}/{etat}")
    public int countColisByEtatAndSocieteLiv(@PathVariable(value = "id") Long id , @PathVariable(value = "etat") String etat )
    {
        return colisService.countColisByEtatAndSocieteLiv(id, etat);
    }


    @GetMapping("/getColisBySocieteLivAndEtat/{id}/{etat}")
    public List<Colis> getColisBySocieteLivAndEtat(@PathVariable(value = "id") Long id , @PathVariable(value = "etat") String etat  )
    {
        return colisService.getColisBySocieteLivAndEtat(id, etat);
    }
}
