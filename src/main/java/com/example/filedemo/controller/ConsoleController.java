package com.example.filedemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.filedemo.model.Console;
import com.example.filedemo.service.ConsoleService;



@RestController
public class ConsoleController {

	@Autowired
    ConsoleService pservice ;
	
	//http://localhost:8080/retrieve-all-Consoles
    @GetMapping("/retrieve-all-Consoles")
    @ResponseBody
    public List<Console> getConsole() {
        List<Console> list = pservice.retrieveAllConsoles();
        return list;
    }

    // http://localhost:8080/retrieve-Console/{Console-id}
    @GetMapping("/retrieve-Console/{Console-id}")
    @ResponseBody
    public Console retrieveConsole(@PathVariable("Console-id") String id) {
        return pservice.retrieveConsole(id);
    }

    // Ajouter  : http://localhost:8080/add-Console
    @PostMapping("/add-Console")
    @ResponseBody
    public Console addConsole(@RequestBody Console p) {
    	Console p1 = pservice.addConsole(p);
        return p1;
    }



    // http://localhost:8080/remove-Console/{id}
    @DeleteMapping("/remove-Console/{id}")
    @ResponseBody
    public void removeConsole(@PathVariable("id") Long id) {
        pservice.deleteConsole(id);
    }

    // http://localhost:8080/modify-Console
    @PutMapping("/modify-Console")
    @ResponseBody
    public Console modifyConsole(@RequestBody Console p) {
        return pservice.updateConsole(p);
    }
}
