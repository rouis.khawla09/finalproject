package com.example.filedemo.controller;

import java.util.List;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.example.filedemo.model.Colis;
import com.example.filedemo.model.PDFGenerator3;
import com.example.filedemo.model.Runsheet;
import com.example.filedemo.service.RunsheetService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@CrossOrigin("*")
@RequestMapping 

public class RunsheetController {

	@Autowired
	RunsheetService pservice ;
	
	//http://localhost:8080/retrieve-all-Runsheets
    @GetMapping("/retrieve-all-Runsheets")
    @ResponseBody
    public List<Runsheet> getRunsheet() {
        List<Runsheet> list = pservice.retrieveAllRunsheets();
        return list;
    }

    // http://localhost:8080/retrieve-Runsheet/{Runsheet-id}
    @GetMapping("/retrieve-Runsheet/{Runsheet-id}")
    @ResponseBody
    public Runsheet retrieveRunsheet(@PathVariable("Runsheet-id") String id) {
        return pservice.retrieveRunsheet(id);
    }

    // Ajouter  : http://localhost:8080/add-Runsheet
    @PostMapping("/add-Runsheet")
    @ResponseBody
    public Runsheet addRunsheet(@RequestBody Runsheet p) {
    	Runsheet p1 = pservice.addRunsheet(p);
        return p1;
    }



    // http://localhost:8080/remove-Runsheet/{id}
    @DeleteMapping("/remove-Runsheet/{id}")
    @ResponseBody
    public void removeRunsheet(@PathVariable("id") Long id) {
        pservice.deleteRunsheet(id);
    }

    // http://localhost:8080/modify-Runsheet
    @PutMapping("/modify-Runsheet")
    @ResponseBody
    public Runsheet modifyRunsheet(@RequestBody Runsheet p) {
        return pservice.updateRunsheet(p);
    }
    
    @GetMapping("/object2")
    // @ResponseBody
     public List<Colis> findByObjectList(@RequestBody  List<Long> inputList  )
     {
     	//List<Long> inputList ; 
     	List<Colis> List1 = pservice.findByObjectList(inputList);
     	return List1 ;
     }
    
    @GetMapping(value = "/runsheet/{code_runsheet}",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity colissReport( @PathVariable(value="code_runsheet")  Long code_runsheet) throws IOException {
		Runsheet  r = pservice.findById(code_runsheet).orElse(new Runsheet()) ;
        ByteArrayInputStream bis = PDFGenerator3.runsheetPDFReport(r);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=colis.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
    
    @RequestMapping("/createBarCodeRun/{code_runsheet}")
    public String generateRunsheetBarCode(@PathVariable(value = "code_runsheet") Long code_runsheet)
     {
    	String text = pservice.generateRunsheetBarCode(code_runsheet);
    	return text ;
     }
    

    @PutMapping(value = "/addColisToRunsheet/{code_runsheet}")
    public String  addColisToRunsheet(@RequestBody  List<String> bar_codeList ,  @PathVariable(value = "code_runsheet") Long code_runsheet  ) throws IOException {
       String text = pservice.addColisToRunsheet(bar_codeList , code_runsheet);
    	return   text ;  
 
    }

    @RequestMapping("/getRunsheetBySocieteLiv/{id}")
    public List<Runsheet> getRunsheetBySocieteLiv(@PathVariable(value = "id") Long id)
    {
       return  pservice.getRunsheetBySocieteLiv(id);

    }
	
}
