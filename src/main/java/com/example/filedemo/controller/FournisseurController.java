package com.example.filedemo.controller;



import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.example.filedemo.model.User;
import com.example.filedemo.repository.UserRepository;
import org.springframework.boot.json.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.FileUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.ServletContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.example.filedemo.model.Fournisseur;
import com.example.filedemo.model.Response;
import com.example.filedemo.repository.FournisseurRepository;
import com.example.filedemo.service.FournisseurService;

@Controller
@RestController
@CrossOrigin("*")
@RequestMapping 
public class FournisseurController {
	
	
	@Autowired 
	private final FournisseurService fournisseurService ;
	
	@Autowired
	ServletContext context;
	
	@Autowired 	
	FournisseurRepository  repository;

	@Autowired
	UserRepository irep;;



	@Autowired  
	public FournisseurController  (FournisseurService fournisseurService)
	{ this.fournisseurService = fournisseurService ;  }



//find by id	
	@RequestMapping("/findFournisseurById/{id}")   
	@ResponseBody
	public Optional<Fournisseur> findById(@PathVariable(value = "id") Long id)
	{
		return fournisseurService.findById(id);
		
	}


    //save 
   @PostMapping(value="saveFournisseur")	
public  Fournisseur saveFournisseur (@RequestBody  Fournisseur  fournisseur  )
{
 System.out.println("  fournisseur save works properly ! ") ; 
 fournisseurService.saveFournisseur(fournisseur);
 return  fournisseur ;
}


//find all by id SL
@GetMapping(value="getFournisseurBySocieteLiv/{id}")
public List<Fournisseur> getFournisseurBySocieteLiv(@PathVariable(value = "id") Long id)
{
  return fournisseurService.getFournisseurBySocieteLiv(id) ;
}
//find all
@GetMapping(value="getAllFournisseur")
public List<Fournisseur> getAllFournisseur()
{
  return fournisseurService.getAllFournisseur() ;
}
//find all fournisseurs isdeleted =false
@GetMapping(value="getAllFournisseurNonDeleted")
public List<Fournisseur> getAllFournisseurNonDeleted()
{
  return fournisseurService.getFournisseurIsDeleted() ;
}

//find all fournisseurs isdeleted =false and idSL
@GetMapping(value="getAllFournisseurNonDeletedByIDSL/{id}")
public List<Fournisseur> getAllFournisseurNonDeletedByIDSL(@PathVariable(value = "id") Long id)
{
return fournisseurService.getFournisseurIsDeletedFalseByIdSL(id) ;
}


@CrossOrigin("*")
@PutMapping("/updatefournisseur")
public  Fournisseur updateFournisseur( @RequestBody   Fournisseur fournisseur )
{  
   return fournisseurService.updateFournisseur( fournisseur);
}



//delete
@GetMapping("deleteLogiqueFournisseur/{id}")
public String deleteFournisseur(@PathVariable(value = "id") Long id)
 {
	fournisseurService.deleteLogiqueFournisseur(id);
	return " fournisseur Deletetd !" ;
 }


@PostMapping("/addFournisseurWithImage")
public ResponseEntity<Response> addFournisseurWithImage (@RequestParam("file") MultipartFile file,
		 @RequestParam("fournisseur") String fournisseur) throws JsonParseException , JsonMappingException , Exception
{

   Fournisseur testfournisseur = new ObjectMapper().readValue(fournisseur, Fournisseur.class);
   boolean isExit = new File(context.getRealPath("/Images/")).exists();
   if (!isExit)
   {
   	new File (context.getRealPath("/Images/")).mkdir();
   }
   String filename = file.getOriginalFilename();
   String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
   File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
   try
   {
   	System.out.println("Image");
   	 FileUtils.writeByteArrayToFile(serverFile,file.getBytes());
   	 
   }catch(Exception e) {
   	e.printStackTrace();
   }

  
   testfournisseur.setLogo(newFileName);
   Fournisseur savedFournisseur = repository.save(testfournisseur);
   if (savedFournisseur != null)
   {
   	return new ResponseEntity<Response>(new Response (""),HttpStatus.OK);
   }
   else
   {
   	return new ResponseEntity<Response>(new Response ("Fournisseur not saved"),HttpStatus.BAD_REQUEST);	
   }
}



	@GetMapping(path="/ImageFournisseur/{id}")
	public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
		Fournisseur fournisseur   = repository.findById(id).get();
		System.out.println("get image done");
		 return Files.readAllBytes(Paths.get(context.getRealPath("/Images/")+fournisseur.getLogo()));
	     }

	@PutMapping("/updateFournisseurPhoto")
	public ResponseEntity<Response> updateFournisseurPhoto (@RequestParam("file") MultipartFile file,
															 @RequestParam("fournisseur") String fournisseur) throws JsonParseException , JsonMappingException , Exception
	{

		Fournisseur testfournisseur = new ObjectMapper().readValue(fournisseur, Fournisseur.class);
		boolean isExit = new File(context.getRealPath("/Images/")).exists();
		if (!isExit)
		{
			new File (context.getRealPath("/Images/")).mkdir();
		}
		String filename = file.getOriginalFilename();
		String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
		File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
		try
		{
			System.out.println("Image");
			FileUtils.writeByteArrayToFile(serverFile,file.getBytes());

		}catch(Exception e) {			e.printStackTrace();
		}


		testfournisseur.setLogo(newFileName);
		Fournisseur savedFournisseur = updateFournisseur(testfournisseur);
		if (savedFournisseur != null)
		{
			return new ResponseEntity<Response>(new Response (""),HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<Response>(new Response ("Fournisseur not saved"),HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/update-fournisseur/{id}")
	private Fournisseur updateEvent(@RequestBody Fournisseur Fournisseur, @PathVariable("id")Long id)
	{
		fournisseurService.updateFournisseur(Fournisseur, id);
		return Fournisseur;
	}

	@PostMapping("/addfour")
	public Fournisseur register(@RequestBody()  Fournisseur four ) throws IOException {


		return fournisseurService.saveFour(four);

	}




	@PutMapping("/changePassWord/{pas}")
	public void azerty( @PathVariable("pas") String pas) {

		//if (this.code != null && this.code.equals(code)) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user0 = irep.findByUsername(auth.getName());
		Long id0=(long) user0.getIduser();
		fournisseurService.changePassWord(id0, pas);

	}


}
