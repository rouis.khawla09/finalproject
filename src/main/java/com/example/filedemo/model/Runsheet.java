package com.example.filedemo.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

public class Runsheet {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long code_runsheet;
	private String bar_code ;
	private LocalDateTime date_creation_runsheet =LocalDateTime.now(); ;
	private float total_prix ;
	private String etat_debrief ;
	

	
	@ManyToOne 
    public Personnel personnel ;
	
	@JsonIgnore
	  @OneToMany(mappedBy = "runsheet")
	  public List<Colis> colis ;
	
	public String toRunsheetBarCode ()
	{
	   
		
	return " " + this.getPersonnel().getHub().getSocieteLiv().getId() +  this.getPersonnel().getIduser() + this.getCode_runsheet()  ;
	

	}

	public Long getCode_runsheet() {
		return code_runsheet;
	}

	public void setCode_runsheet(Long code_runsheet) {
		this.code_runsheet = code_runsheet;
	}

	public String getBar_code() {
		return bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	public LocalDateTime getDate_creation_runsheet() {
		return date_creation_runsheet;
	}

	public void setDate_creation_runsheet(LocalDateTime date_creation_runsheet) {
		this.date_creation_runsheet = date_creation_runsheet;
	}

	public float getTotal_prix() {
		return total_prix;
	}

	public void setTotal_prix(float total_prix) {
		this.total_prix = total_prix;
	}

	public String getEtat_debrief() {
		return etat_debrief;
	}

	public void setEtat_debrief(String etat_debrief) {
		this.etat_debrief = etat_debrief;
	}

	public Personnel getPersonnel() {
		return personnel;
	}

	public void setPersonnel(Personnel personnel) {
		this.personnel = personnel;
	}

	public List<Colis> getColis() {
		return colis;
	}

	public void setColis(List<Colis> colis) {
		this.colis = colis;
	}

	public Runsheet(Long code_runsheet, String bar_code, LocalDateTime date_creation_runsheet, float total_prix,
			String etat_debrief, Personnel personnel, List<Colis> colis) {
		super();
		this.code_runsheet = code_runsheet;
		this.bar_code = bar_code;
		this.date_creation_runsheet = date_creation_runsheet;
		this.total_prix = total_prix;
		this.etat_debrief = etat_debrief;
		this.personnel = personnel;
		this.colis = colis;
	}
	
	public Runsheet() {}

	@Override
	public String toString() {
		return "Runsheet [code_runsheet=" + code_runsheet + ", bar_code=" + bar_code + ", date_creation_runsheet="
				+ date_creation_runsheet + ", total_prix=" + total_prix + ", etat_debrief=" + etat_debrief
				+ ", personnel=" + personnel + ", colis=" + colis + "]";
	}
	
	
}
