package com.example.filedemo.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

@Inheritance(strategy=InheritanceType.JOINED)

public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long iduser;
	//private String nom;
	private String username;
	private String Tel;
	private String email;
//	@Temporal(TemporalType.DATE)
//	private Date datefincontrat;
//	private String nomsociete ;
//	private String gouvernerasociete;
//	private String adresselivraion;
//	private String delgationsociete;
//	private String localitesociete;
//	private int codepostalesociete;
	private String image;
//	private String adresselivraison;
//	private String gouverneralivraison;
//	private String localitelivraison;
//	private String Codepostalelivraision;
//	private float prixlivraision;
//	private float prixretour;
	private String password;
	private Roles roleUser;
//	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
//	private List<Role> role = new ArrayList<>();
	public long getIduser() {
		return iduser;
	}
	public void setIduser(long iduser) {
		this.iduser = iduser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return Tel;
	}
	public void setTel(String tel) {
		Tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Roles getRoleUser() {
		return roleUser;
	}
	public void setRoleUser(Roles roleUser) {
		this.roleUser = roleUser;
	}
	public User(long iduser, String username, String tel, String email, String image, String password, Roles roleUser) {
		super();
		this.iduser = iduser;
		this.username = username;
		Tel = tel;
		this.email = email;
		this.image = image;
		this.password = password;
		this.roleUser = roleUser;
	}
	
	public User(){}
	@Override
	public String toString() {
		return "User [iduser=" + iduser + ", username=" + username + ", Tel=" + Tel + ", email=" + email + ", image="
				+ image + ", password=" + password + ", roleUser=" + roleUser + "]";
	}
	
	
}
