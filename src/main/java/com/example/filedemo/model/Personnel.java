package com.example.filedemo.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@PrimaryKeyJoinColumn(name = "iduser")
public class Personnel extends User {

//	@Id
//	@GeneratedValue (strategy = GenerationType.IDENTITY)
//	private Long id ;
	private Long cin ;
	private String nom ;
	private String prenom ;
	private role role_personnel ;
	private int tel_personnel ;
	@Email
	private String mail ; 
	private String permis ; 
	
	private String matricule_veh ;
	private String carte_grise ; 
	private String photo ; 

	
	@ManyToOne
    public Hub hub ;
	
	
	public enum role {
        livreur,
        magasinier,
        commercial,
        gérant;
    }


	public Long getCin() {
		return cin;
	}


	public void setCin(Long cin) {
		this.cin = cin;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public role getRole_personnel() {
		return role_personnel;
	}


	public void setRole_personnel(role role_personnel) {
		this.role_personnel = role_personnel;
	}


	public int getTel_personnel() {
		return tel_personnel;
	}


	public void setTel_personnel(int tel_personnel) {
		this.tel_personnel = tel_personnel;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPermis() {
		return permis;
	}


	public void setPermis(String permis) {
		this.permis = permis;
	}


	public String getMatricule_veh() {
		return matricule_veh;
	}


	public void setMatricule_veh(String matricule_veh) {
		this.matricule_veh = matricule_veh;
	}


	public String getCarte_grise() {
		return carte_grise;
	}


	public void setCarte_grise(String carte_grise) {
		this.carte_grise = carte_grise;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public Hub getHub() {
		return hub;
	}


	public void setHub(Hub hub) {
		this.hub = hub;
	}


	public Personnel(long iduser, String username, String tel, String email, String image, String password,
			Roles roleUser, Long cin, String nom, String prenom, role role_personnel, int tel_personnel,
			@Email String mail, String permis, String matricule_veh, String carte_grise, String photo, Hub hub) {
		super(iduser, username, tel, email, image, password, roleUser);
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.role_personnel = role_personnel;
		this.tel_personnel = tel_personnel;
		this.mail = mail;
		this.permis = permis;
		this.matricule_veh = matricule_veh;
		this.carte_grise = carte_grise;
		this.photo = photo;
		this.hub = hub;
	}
	
	public Personnel() {}


	@Override
	public String toString() {
		return "Personnel [cin=" + cin + ", nom=" + nom + ", prenom=" + prenom + ", role_personnel=" + role_personnel
				+ ", tel_personnel=" + tel_personnel + ", mail=" + mail + ", permis=" + permis + ", matricule_veh="
				+ matricule_veh + ", carte_grise=" + carte_grise + ", photo=" + photo + ", hub=" + hub + "]";
	}
	
	
}
