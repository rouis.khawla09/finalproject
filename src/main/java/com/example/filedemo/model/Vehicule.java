package com.example.filedemo.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vehicule {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id ;
	private String marque ;
	private String matricule ;
	private String carte_grise ;
	
	@ManyToOne 
    public SocieteLiv sociétéLiv ;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Vehicule(int id, String marque, String matricule, String carte_grise) {
		super();
		this.id = id;
		this.marque = marque;
		this.matricule = matricule;
		this.carte_grise = carte_grise;
		
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getCarte_grise() {
		return carte_grise;
	}

	public void setCarte_grise(String carte_grise) {
		this.carte_grise = carte_grise;
	}

	public SocieteLiv getSociétéLiv() {
		return sociétéLiv;
	}

	public void setSociétéLiv(SocieteLiv sociétéLiv) {
		this.sociétéLiv = sociétéLiv;
	}

	
	
	
}
