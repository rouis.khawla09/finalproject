package com.example.filedemo.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
public class Colis {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
		public Long   reference ; 
		
		public String bar_code ; 
		public String nom_c ;
		public String prenom_c ;
		public int tel_c_1;
		public int tel_c_2;
		LocalDateTime date_creation = LocalDateTime.now();
		public String adresse ; 
		public String gouvernorat ; 
		public String delegation ;
		public String localite;
		public int code_postal ; 
		public float  cod ;
		public float latitude ;
		public float longitude ;
		public String  mode_paiement;
		public String   service  ;
		public String designation  ;
		public String remarque ;
		@Audited
		public String  etat ;
		public String anomalie ;
		public Integer  nb_p ;
		public Integer longeur ;
		public Integer  largeur ;
		public Integer  hauteur ;
		public Integer  poids ;
		
		
		@ManyToOne
	    public Fournisseur fournisseur ;
		
		@ManyToOne
	    public Hub hub ;
		
		@ManyToOne
	    public Runsheet runsheet ;
		
		public String toColisBarCode ()
		{
			int serviceCode ; 
			if (new String(this.service).equals("Livraison"))
				{serviceCode = 1;}
			else 
				{serviceCode = 2 ;}
			
			return "010"+ serviceCode + this.fournisseur.getIduser() +this.reference ;
			
		}

		public Long getReference() {
			return reference;
		}

		public void setReference(Long reference) {
			this.reference = reference;
		}

		public String getBar_code() {
			return bar_code;
		}

		public void setBar_code(String bar_code) {
			this.bar_code = bar_code;
		}

		public String getNom_c() {
			return nom_c;
		}

		public void setNom_c(String nom_c) {
			this.nom_c = nom_c;
		}

		public String getPrenom_c() {
			return prenom_c;
		}

		public void setPrenom_c(String prenom_c) {
			this.prenom_c = prenom_c;
		}

		public int getTel_c_1() {
			return tel_c_1;
		}

		public void setTel_c_1(int tel_c_1) {
			this.tel_c_1 = tel_c_1;
		}

		public int getTel_c_2() {
			return tel_c_2;
		}

		public void setTel_c_2(int tel_c_2) {
			this.tel_c_2 = tel_c_2;
		}

		public LocalDateTime getDate_creation() {
			return date_creation;
		}

		public void setDate_creation(LocalDateTime date_creation) {
			this.date_creation = date_creation;
		}

		public String getAdresse() {
			return adresse;
		}

		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}

		public String getGouvernorat() {
			return gouvernorat;
		}

		public void setGouvernorat(String gouvernorat) {
			this.gouvernorat = gouvernorat;
		}

		public String getDelegation() {
			return delegation;
		}

		public void setDelegation(String delegation) {
			this.delegation = delegation;
		}

		public String getLocalite() {
			return localite;
		}

		public void setLocalite(String localite) {
			this.localite = localite;
		}

		public int getCode_postal() {
			return code_postal;
		}

		public void setCode_postal(int code_postal) {
			this.code_postal = code_postal;
		}

		public float getCod() {
			return cod;
		}

		public void setCod(float cod) {
			this.cod = cod;
		}

		public float getLatitude() {
			return latitude;
		}

		public void setLatitude(float latitude) {
			this.latitude = latitude;
		}

		public float getLongitude() {
			return longitude;
		}

		public void setLongitude(float longitude) {
			this.longitude = longitude;
		}

		public String getMode_paiement() {
			return mode_paiement;
		}

		public void setMode_paiement(String mode_paiement) {
			this.mode_paiement = mode_paiement;
		}

		public String getService() {
			return service;
		}

		public void setService(String service) {
			this.service = service;
		}

		public String getDesignation() {
			return designation;
		}

		public void setDesignation(String designation) {
			this.designation = designation;
		}

		public String getRemarque() {
			return remarque;
		}

		public void setRemarque(String remarque) {
			this.remarque = remarque;
		}

		public String getEtat() {
			return etat;
		}

		public void setEtat(String etat) {
			this.etat = etat;
		}

		public String getAnomalie() {
			return anomalie;
		}

		public void setAnomalie(String anomalie) {
			this.anomalie = anomalie;
		}

		public Integer getNb_p() {
			return nb_p;
		}

		public void setNb_p(Integer nb_p) {
			this.nb_p = nb_p;
		}

		public Integer getLongeur() {
			return longeur;
		}

		public void setLongeur(Integer longeur) {
			this.longeur = longeur;
		}

		public Integer getLargeur() {
			return largeur;
		}

		public void setLargeur(Integer largeur) {
			this.largeur = largeur;
		}

		public Integer getHauteur() {
			return hauteur;
		}

		public void setHauteur(Integer hauteur) {
			this.hauteur = hauteur;
		}

		public Integer getPoids() {
			return poids;
		}

		public void setPoids(Integer poids) {
			this.poids = poids;
		}

		public Fournisseur getFournisseur() {
			return fournisseur;
		}

		public void setFournisseur(Fournisseur fournisseur) {
			this.fournisseur = fournisseur;
		}

		public Hub getHub() {
			return hub;
		}

		public void setHub(Hub hub) {
			this.hub = hub;
		}

		public Runsheet getRunsheet() {
			return runsheet;
		}

		public void setRunsheet(Runsheet runsheet) {
			this.runsheet = runsheet;
		}

		public Colis(Long reference, String bar_code, String nom_c, String prenom_c, int tel_c_1, int tel_c_2,
				LocalDateTime date_creation, String adresse, String gouvernorat, String delegation, String localite,
				int code_postal, float cod, float latitude, float longitude, String mode_paiement, String service,
				String designation, String remarque, String etat, String anomalie, Integer nb_p, Integer longeur,
				Integer largeur, Integer hauteur, Integer poids, Fournisseur fournisseur, Hub hub, Runsheet runsheet) {
			super();
			this.reference = reference;
			this.bar_code = bar_code;
			this.nom_c = nom_c;
			this.prenom_c = prenom_c;
			this.tel_c_1 = tel_c_1;
			this.tel_c_2 = tel_c_2;
			this.date_creation = date_creation;
			this.adresse = adresse;
			this.gouvernorat = gouvernorat;
			this.delegation = delegation;
			this.localite = localite;
			this.code_postal = code_postal;
			this.cod = cod;
			this.latitude = latitude;
			this.longitude = longitude;
			this.mode_paiement = mode_paiement;
			this.service = service;
			this.designation = designation;
			this.remarque = remarque;
			this.etat = etat;
			this.anomalie = anomalie;
			this.nb_p = nb_p;
			this.longeur = longeur;
			this.largeur = largeur;
			this.hauteur = hauteur;
			this.poids = poids;
			this.fournisseur = fournisseur;
			this.hub = hub;
			this.runsheet = runsheet;
		}
		
		public Colis() {}

		@Override
		public String toString() {
			return "Colis [reference=" + reference + ", bar_code=" + bar_code + ", nom_c=" + nom_c + ", prenom_c="
					+ prenom_c + ", tel_c_1=" + tel_c_1 + ", tel_c_2=" + tel_c_2 + ", date_creation=" + date_creation
					+ ", adresse=" + adresse + ", gouvernorat=" + gouvernorat + ", delegation=" + delegation
					+ ", localite=" + localite + ", code_postal=" + code_postal + ", cod=" + cod + ", latitude="
					+ latitude + ", longitude=" + longitude + ", mode_paiement=" + mode_paiement + ", service="
					+ service + ", designation=" + designation + ", remarque=" + remarque + ", etat=" + etat
					+ ", anomalie=" + anomalie + ", nb_p=" + nb_p + ", longeur=" + longeur + ", largeur=" + largeur
					+ ", hauteur=" + hauteur + ", poids=" + poids + ", fournisseur=" + fournisseur + ", hub=" + hub
					+ ", runsheet=" + runsheet + "]";
		}
		
		
}
