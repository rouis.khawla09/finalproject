package com.example.filedemo.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

public class Hub {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id_hub ;
	private String gouvernorat ;
	private String titre ;
	private String adresse ;
	@ElementCollection(targetClass=	String.class)
	public List<String> gouvernorat_lie = new ArrayList<String>();
	
	@ManyToOne 
    public SocieteLiv societeLiv ;
	
	@JsonIgnore
	@OneToMany(mappedBy = "hub")
	public List<Colis> colis ;
	
	@JsonIgnore
	@OneToMany(mappedBy = "hub")
	public List<Personnel> personnel ;

	public Long getId_hub() {
		return id_hub;
	}

	public void setId_hub(Long id_hub) {
		this.id_hub = id_hub;
	}

	public String getGouvernorat() {
		return gouvernorat;
	}

	public void setGouvernorat(String gouvernorat) {
		this.gouvernorat = gouvernorat;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<String> getGouvernorat_lie() {
		return gouvernorat_lie;
	}

	public void setGouvernorat_lie(List<String> gouvernorat_lie) {
		this.gouvernorat_lie = gouvernorat_lie;
	}

	public SocieteLiv getSocieteLiv() {
		return societeLiv;
	}

	public void setSocieteLiv(SocieteLiv societeLiv) {
		this.societeLiv = societeLiv;
	}

	public List<Colis> getColis() {
		return colis;
	}

	public void setColis(List<Colis> colis) {
		this.colis = colis;
	}

	public List<Personnel> getPersonnel() {
		return personnel;
	}

	public void setPersonnel(List<Personnel> personnel) {
		this.personnel = personnel;
	}

	public Hub(Long id_hub, String gouvernorat, String titre, String adresse, List<String> gouvernorat_lie,
			SocieteLiv societeLiv, List<Colis> colis, List<Personnel> personnel) {
		super();
		this.id_hub = id_hub;
		this.gouvernorat = gouvernorat;
		this.titre = titre;
		this.adresse = adresse;
		this.gouvernorat_lie = gouvernorat_lie;
		this.societeLiv = societeLiv;
		this.colis = colis;
		this.personnel = personnel;
	}
	
	public Hub() {}

	@Override
	public String toString() {
		return "Hub [id_hub=" + id_hub + ", gouvernorat=" + gouvernorat + ", titre=" + titre + ", adresse=" + adresse
				+ ", gouvernorat_lie=" + gouvernorat_lie + ", societeLiv=" + societeLiv + ", colis=" + colis
				+ ", personnel=" + personnel + "]";
	}
	
	
	
}
