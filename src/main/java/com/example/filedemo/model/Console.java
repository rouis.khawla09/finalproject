package com.example.filedemo.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Console {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id_console ;
	private Date date_creation ;
	private String depart ;
	private String arrivee ;
	private String etat ;
	
	@ManyToOne 
    public Personnel personnel ;
	
	@ManyToOne 
    public Colis colis ;
}
