package com.example.filedemo.model;



import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
public class SocieteLiv {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id ;
	private String nom ;
	private String patente ;
	private String mat_fisc ;
	private String mail ;
	private String adresse ;
	private String localisation ;
	private int tel ;
	
	@JsonIgnore
	@ManyToOne 
	public User user ;
	
	 @JsonIgnore
	 @OneToMany(mappedBy = "societeLiv")
	 public List<Fournisseur> fournisseur ;
	 
	 @JsonIgnore
	 @OneToMany(mappedBy = "societeLiv")
	 public List<Hub> hub ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getMat_fisc() {
		return mat_fisc;
	}

	public void setMat_fisc(String mat_fisc) {
		this.mat_fisc = mat_fisc;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Fournisseur> getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(List<Fournisseur> fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<Hub> getHub() {
		return hub;
	}

	public void setHub(List<Hub> hub) {
		this.hub = hub;
	}

	public SocieteLiv(Long id, String nom, String patente, String mat_fisc, String mail, String adresse,
			String localisation, int tel, User user, List<Fournisseur> fournisseur, List<Hub> hub) {
		super();
		this.id = id;
		this.nom = nom;
		this.patente = patente;
		this.mat_fisc = mat_fisc;
		this.mail = mail;
		this.adresse = adresse;
		this.localisation = localisation;
		this.tel = tel;
		this.user = user;
		this.fournisseur = fournisseur;
		this.hub = hub;
	}
	 
	 
	public SocieteLiv() {}

	@Override
	public String toString() {
		return "SocieteLiv [id=" + id + ", nom=" + nom + ", patente=" + patente + ", mat_fisc=" + mat_fisc + ", mail="
				+ mail + ", adresse=" + adresse + ", localisation=" + localisation + ", tel=" + tel + ", user=" + user
				+ ", fournisseur=" + fournisseur + ", hub=" + hub + "]";
	}
	
	
	 
}
