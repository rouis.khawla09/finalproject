package com.example.filedemo.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dispatch {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id_dispatch ;
	private Date date_creation ;
	
	@ManyToOne
	private Personnel personnel;
	
	
}
