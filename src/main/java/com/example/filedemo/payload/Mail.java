package com.example.filedemo.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public class Mail {

	private String recipient;
    private String subject;
    private String message;
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Mail(String recipient, String subject, String message) {
		super();
		this.recipient = recipient;
		this.subject = subject;
		this.message = message;
	}
    
    public Mail() {}
	@Override
	public String toString() {
		return "Mail [recipient=" + recipient + ", subject=" + subject + ", message=" + message + "]";
	}
    
    
}
