package com.example.filedemo.payload;

import java.io.Serializable;

import com.example.filedemo.model.Roles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public class JwtRequest implements Serializable {

private static final long serialVersionUID = 5926468583005150707L;
	
	private String token;
	private String username;
	private String password;
	private String Tel;
	private String email;
	private String image;
	private Roles roleUser;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTel() {
		return Tel;
	}
	public void setTel(String tel) {
		Tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Roles getRoleUser() {
		return roleUser;
	}
	public void setRoleUser(Roles roleUser) {
		this.roleUser = roleUser;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public JwtRequest(String token, String username, String password, String tel, String email, String image,
			Roles roleUser) {
		super();
		this.token = token;
		this.username = username;
		this.password = password;
		Tel = tel;
		this.email = email;
		this.image = image;
		this.roleUser = roleUser;
	}

	public JwtRequest() {}
	@Override
	public String toString() {
		return "JwtRequest [token=" + token + ", username=" + username + ", password=" + password + ", Tel=" + Tel
				+ ", email=" + email + ", image=" + image + ", roleUser=" + roleUser + "]";
	}
	
	
	
}
