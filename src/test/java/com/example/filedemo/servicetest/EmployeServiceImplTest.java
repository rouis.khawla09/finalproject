package com.example.filedemo.servicetest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import org.springframework.test.context.junit4.SpringRunner;

import com.example.filedemo.model.Employe;
import com.example.filedemo.repository.EmployeRepository;
import com.example.filedemo.service.IEmployeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeServiceImplTest {
	@Autowired
	IEmployeService iemployeservice;

	@Autowired
	EmployeRepository empRepo;


	@Test
	public void testAjouterEmploye() {
		Employe employe = new Employe(1, "souhaiel", "benaissa", "benaissasouhaiel@gmail.com");
		int a = iemployeservice.ajouterEmploye(employe);
		assertEquals(employe.getId(), a);
	}

	
	@Test
	public void testGetNombreEmployeJPQL() {
		int count = iemployeservice.getNombreEmployeJPQL();
		assertNotEquals(1, count);
	}

	@Test
	public void testGetAllEmployeNamesJPQL() {
		List<Employe> list = (List<Employe>) empRepo.findAll();
		List<String> noms = new ArrayList<>();
		for (Employe emp : list) {
			noms.add(emp.getNom());
		}
		System.out.println(noms);
		List<String> empJPQL = iemployeservice.getAllEmployeNamesJPQL();
		System.out.println(empJPQL);
		assertEquals(empJPQL.size(), list.size());
	}


	@Test
	public void testGetAllEmployes() {
		List<Employe> employes = iemployeservice.getAllEmployes();
		assertNotEquals(1, employes.size());
	}


	
}
