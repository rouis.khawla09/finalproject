FROM openjdk:8-jdk-alpine
EXPOSE 8086
ADD target/file-demo-1.0.war file-demo-1.0.war
ENTRYPOINT ["java","-jar","/file-demo-1.0.war"]
